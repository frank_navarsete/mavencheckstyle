# Use of maven checkstyle

This is a simple project to check the code during build time.

## How it works

Use the [maven-checkstyle-plugin](https://maven.apache.org/plugins/maven-checkstyle-plugin/index.html).

In the pom.xml we enable the plugin during build. In the root directory we include a custom checkstyle-file "checkstyle-custom.xml"
```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-checkstyle-plugin</artifactId>
            <version>3.1.1</version>
            <configuration>
                <configLocation>checkstyle.custom.xml</configLocation>
                <encoding>UTF-8</encoding>
                <consoleOutput>true</consoleOutput>
                <failsOnError>true</failsOnError>
                <linkXRef>false</linkXRef>
            </configuration>
            <executions>
                <execution>
                    <id>checkstyle-validate</id>
                    <phase>validate</phase>
                    <goals>
                        <goal>check</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```


## Usage
Use of the regular maven command and the maven plugin will trigger during validate phase.
```bash
mvn clean install
```
